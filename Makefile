include $(FSLCONFDIR)/default.mk

PROJNAME   = siena
XFILES     = siena_diff
SCRIPTS    = siena siena_flirt siena_cal sienax siena_flow2std \
             viena_quant viena_createpng
LIBS      = -lfsl-newimage -lfsl-miscmaths -lfsl-NewNifti -lfsl-znz \
            -lfsl-cprob -lfsl-utils

all: ${XFILES}

siena_diff: siena_diff.o
	${CXX} ${CXXFLAGS} -o $@ $^ ${LDFLAGS}
